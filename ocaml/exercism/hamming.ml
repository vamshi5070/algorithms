open Base

type nucleotide = A | C | G | T

let rec hamming_distance (fst: nucleotide list) (snd: nucleotide list) (cnt: int): (int,string) Result.t = 
        match (fst ,snd) with
        | ([], []) -> Ok (cnt)
        | ([], _) -> Error "left strand must not be empty"
        | (_, []) -> Error "right strand must not be empty"
        | (x :: rest1, y :: rest2) when x = y -> hamming_distance rest1 rest2 (cnt+ 1)
;;


        
