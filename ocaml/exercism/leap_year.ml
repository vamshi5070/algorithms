let leap_year num = num mod 400 = 0 || not (num mod 100 = 0) && (num mod 4 = 0) ;;

let () = print_endline (string_of_bool (leap_year 2001));;
