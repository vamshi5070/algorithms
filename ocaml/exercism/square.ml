(*val sum_of_list : int list -> int*)
let square num = num * num;;

let sum_of_list lst = List.fold_left (fun acc x -> acc + x) 0 lst
;;

let rec generate_num n = 
        if n <= 0 then
                []
        else
                generate_num (n - 1) @ [n];;

let sum_of_square num = let lst = generate_num num in
        sum_of_list (List.map square lst);;

let square_of_sum num = let lst = generate_num num in
        square (sum_of_list lst);;

let difference_of_squares num = square_of_sum num - sum_of_square num;;

let () = print_int (difference_of_squares 5);;
