{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        # packages.hello = pkgs.hello;
        devShell = pkgs.mkShell {
          buildInputs = [
          # pkgs.cowsay#pkgs.hello
          pkgs.ghc
          pkgs.clang
          pkgs.gcc
          pkgs.python39
          pkgs.rustc
          pkgs.exercism
          pkgs.ocaml
          pkgs.ocamlPackages.utop
          pkgs.ocamlPackages.base
          pkgs.pkg-config
          pkgs.hello
          pkgs.SDL2
          # pkgs.haskell-language-server
          # pkgs.cabal-install
          ];
          VAMSHI = "${pkgs.hello}";
          PKG_CONFIG_PATH = "${pkgs.SDL2}";
          
        };
      });
}
