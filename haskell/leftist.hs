data Heap a = Empty | Node Int a (Heap a) (Heap a)
    deriving (Eq,Show)

empty :: Heap a
empty = Empty

singleton :: (Ord a) => a -> Heap a
singleton x = Node 1 x Empty Empty

isEmpty :: Heap a -> Bool
isEmpty Empty = True
isEmpty _ = False

--minInsert :: (Ord a) => a -> Heap a -> Heap a
--minInsert x 

merge :: (Ord a) => Heap a -> Heap a -> Heap a 
merge Empty sndHeap = sndHeap
merge fstHeap Empty = fstHeap
merge t1@(Node rank1 x1 l1 r1) t2@(Node rank2 x2 l2 r2)  
    | x1 <= x2 = makeT x1 l1 (merge r1 t2) 
    | otherwise = makeT x2 l2 (merge r2 t1) 

rank :: Heap a -> Int
rank Empty = 0
rank (Node r _ _ _) = r

makeT :: (Ord a) => a -> Heap a -> Heap a -> Heap a
makeT x t1 t2 
    | rank t1 >= rank t2 = Node (rank t2 + 1) x t1 t2
    | otherwise = Node (rank t1 + 1) x t2 t1

insert :: (Ord a) => a -> Heap a -> Heap a
insert x t = merge (Node 1 x Empty Empty) t

divideAndConquer :: (Ord a) => [Heap a] -> [Heap a]
divideAndConquer [] = []
divideAndConquer [x] = [x]
divideAndConquer [x,y] = [merge x y]
divideAndConquer hs = mergeHeap (divideAndConquer xs) (divideAndConquer ys)
    where (xs,ys) = splitAt (length hs `div` 2) hs

mergeHeap :: (Ord a) => [Heap a] -> [Heap a] -> [Heap a]
mergeHeap [] xs = xs
mergeHeap xs [] = xs
mergeHeap (x:_) (y:_) = [merge x y]

insert' :: (Ord a) => a -> Heap a -> Heap a
insert' key Empty = Node 1 key Empty Empty
insert' key (Node rank' x l r) 
    | key <= x = makeT key (l' x) r'
    | otherwise = makeT x (l' key) r'
    where l' y = if ((rank l) >= (rank r)) then (insert' y r) else (insert' y l) 
          r'  =  if ((rank l) >= (rank r)) then l else r

findMin :: (Ord a) => Heap a -> a
findMin (Node _ x _ _) = x

deleteMin :: (Ord a) => Heap a -> Heap a
deleteMin Empty = Empty
deleteMin (Node rank x l r) = merge l r

fromList :: (Ord a) => [a] -> Heap a
fromList xs = foldr insert empty xs

size :: Heap a -> Int
size Empty = 0
size (Node s _ _ _) = s

makeT' :: (Ord a) => a -> Heap a -> Heap a -> Heap a
makeT' x t1 t2 
    | size t1 >= size t2 = Node (size t1 + size t2 + 1) x t1 t2
    | otherwise = Node (size t1 + size t2 + 1) x t2 t1


merge' :: (Ord a) => Heap a -> Heap a -> Heap a 
merge' Empty sndHeap = sndHeap
merge' fstHeap Empty = fstHeap
merge' t1@(Node size1 x1 l1 r1) t2@(Node size2 x2 l2 r2)  
    | x1 <= x2 = makeT x1 (merge' r1 t2) l1
    | otherwise = makeT x2 (merge' r2 t1) l2

merge'' :: (Ord a) => Heap a -> Heap a -> Heap a 
merge'' Empty sndHeap = sndHeap
merge'' fstHeap Empty = fstHeap
merge'' t1@(Node size1 x1 l1 r1) t2@(Node size2 x2 l2 r2)  
    | x1 <= x2 = if size l1 >= size2 + size r1 then 
        Node (size1 + size2) x1 l1 (merge'' t2 r1)
        else  Node (size1 + size2) x1 (merge'' t2 r1) l1 
    | otherwise = if size l2 >= size1 + size r2 then 
        Node (size1 + size2) x2 l2 (merge'' t1 r2)
        else  Node (size1 + size2) x2 (merge'' t1 r2) l2

insert'' x h = merge'' (Node 1 x Empty Empty) h

fromList'' :: (Ord a) => [a] -> Heap a
fromList'' xs = foldr insert'' empty xs


--    | size1 >= size2 = Node (size1 + 1) (min x1 x2) (merge'' ) 

