natural :: Integral a => [a]
natural = 1: (map (+1) natural)

fib :: Integral a => [a]
fib = 0:1: zipWith (+) fib (tail fib)

-- prime :: Integral a => a -> [a]
-- prime num = [ n  n <- 1..num  ]
