scoreLetter :: Char -> Integer
scoreLetter ch = foldr (\x acc -> if (ch `elem` fst x) then snd x else acc ) 0 ruleBook

ruleBook :: [(String,Integer)]
ruleBook = [("AEIOULNRST" , 1)
           ,("DG" , 2)
           ,("BCMP" , 3)
           ,("FHVWY" , 4)
           ,( "K" , 5)
           ,("JX" , 8)
           ,("QZ" , 10)
           ]

scoreWord :: String -> Integer
scoreWord = sum .  map scoreLetter 
