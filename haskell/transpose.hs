transpose' :: [String] -> [String]
transpose' []  = []
transpose' ([]:xss) = transpose' xss
transpose' xss = (map head xss): transpose' (map tail xss)

maxLength :: [[a]] -> Int
maxLength = maximum . map length 

fillSpaces :: Int -> [String] -> [String]
fillSpaces len = map (\x -> x <> replicate (len - length x) ' ')

transpose'' :: [String] -> [String]
transpose'' rows =  transpose' $ fillSpaces len rows
  where len = maxLength rows

transpose :: [String] -> [String]
transpose xss = fst $ foldr (\x (lst,len) -> if (length (trimLast x) >= len) then (trimLast x:lst,length (trimLast x)) else ((take len x):lst,len)) ([],0) (transpose'' xss)

trimLast :: String -> String
trimLast = fst . foldr (\x acc -> if ((snd acc == False) && (x == ' ')) then acc else (x:fst acc,True) ) ([],False)

ex3 = [ "The fourth line."
  , "The fifth line."
    ]

ex1 = ["T","EE","AAA","SSSS","EEEEE","RRRRRR"]

ex2 = [ "The first line."
     , "The second line."
                  ]

ex4 = ["TAAA", "h   ", "elll", " ooi", "lnnn", "ogge", "n e.", "glr", "ei ", "snl", "tei", " .n", "l e", "i .", "n", "e", "."]

ex5 = [ "The longest line."
      , "A long line."
      , "A longer line."
      , "A line."
           ]

ex6 = [ "T"
                        , "EE"
                        , "AAA"
                        , "SSSS"
                        , "EEEEE"
                        , "RRRRRR"
                        ]

ex7 = [ "FRACTURE"
                        , "OUTLINED"
                        , "BLOOMING"
                        , "SEPTETTE"
                        ]

ex8 = [ "HEART"
                        , "EMBER"
                        , "ABUSE"
                        , "RESIN"
                        , "TREND"
                        ]

-- (Int,Int)
 -- ((take (length (trimLast x) - acc) (trimLast x)):lst,   )) ([],0) xss


-- maximum . map length 

-- transpose :: [String] -> [String]
-- transpose rows = transpose' $ fillSpaces (len,ind) rows
--   where (len,ind) = maxLength rows

 -- foldr (\x acc -> if length x > fst acc then (length x,((snd acc) - 1)) else acc) (0,length xss)  xss
-- mapWhile :: (a -> Bool) -> [a] ->  [a]
-- mapWhile p xs
  -- | 
 -- (\x -> x <> replicate (len - length x) ' ' )
mapWhile :: Int -> (a -> a) -> [a] ->  [a]
mapWhile _ _ [] =[]
mapWhile 0 _ xs = xs
mapWhile n f (x:xs) = f x : mapWhile (n-1) f xs
