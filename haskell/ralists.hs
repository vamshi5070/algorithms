-- random access lists
-- access element k

type Nat = Int

data Tree a = Leaf a | Node  Nat (Tree a) (Tree a)
  deriving (Eq,Show)


size :: Tree a -> Nat
size (Leaf x) = 1
size (Node n _ _) = n 

node :: Tree a -> Tree a -> Tree a
node t1 t2 = Node (size t1 + size t2) t1 t2

data Digit a = Zero | One (Tree a)

type RAList a = [Digit a]

fromRA  :: RAList a -> [a]
fromRA = concatMap from
  where from (Zero) = []
        from (One t)= fromT t
        
fromT :: Tree a -> [a]
fromT (Leaf x) = [x]
fromT (Node _ t1 t2) = fromT t1 <> fromT t2

fetchRA :: Nat -> RAList a -> a
fetchRA k (Zero:rs) = fetchRA k rs
fetchRA k ((One x):rs) = if (k < size x)
                         then  fetchT k x
                         else fetchRA (k - size x) rs

fetchT :: Nat -> Tree a -> a
fetchT 0 (Leaf x) = x
fetchT k (Node n t1 t2) = if (k > mid)
                          then fetchT k t1
                          else fetchT (k - mid) t2
  where mid = n `div` 2
  

nullRA :: RAList a -> Bool
nullRA [] = True
nullRA (Zero:xs) = nullRA xs
nullRA _ = False

nilRA :: RAList a
nilRA = []

consRA :: a -> RAList a -> RAList a
consRA x xs = consT (Leaf x) 
