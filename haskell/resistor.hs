data Color =
  Black
  | Brown
  | Red
  | Orange
  | Yellow
  | Green
  | Blue
  | Violet
  | Grey
  | White
  deriving (Eq,Show,Enum,Bounded)

value :: (Color, Color) -> Int
value (x,y) = x' <> y'
  x' = show $ values x
  y' = show $ values y

values :: Color -> Int
values Black = 0
values Brown = 1
values Red = 2
values Orange = 3
values Yellow = 4
values Green = 5
values Blue = 6
values Violet = 7
values Grey = 8
values White = 9
