
-- suffixes looks elegant, but it is O(n ^ 2)
-- suffixes xs =  (until null helper id ) []
  -- where helper func = func . last


-- suffixes with O(n)
-- suffixes' xs = until (null . snd) (\(acc,x) -> )
suffixes' [] = [[]]
suffixes' (x:xs) = xs: suffixes' xs
 -- map (x:) $ suffixes' xs

 -- ()
