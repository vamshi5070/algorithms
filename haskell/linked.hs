data LinkedList a =  Empty | Node a (LinkedList a)
  deriving (Eq,Show)

datum (Empty) =  error "empty"
datum (Node x _) = x

fromList :: [a] ->  LinkedList a
fromList xs = foldr new nil xs

isNil :: LinkedList a -> Bool
isNil Empty = True
isNil _ = False

new :: a -> LinkedList a -> LinkedList a
new x linkedList = Node x linkedList

next :: LinkedList a -> LinkedList a
next Empty = error "not found"
next (Node _ xs) = xs

nil :: LinkedList a
nil = Empty

reverseLinkedList :: LinkedList a -> LinkedList a
reverseLinkedList  xs =  helper Empty xs
  where helper acc     Empty    = acc
        helper acc  (Node x xs) = helper (Node x acc) xs

toList :: LinkedList a -> [a]
toList Empty = []
toList (Node x xs) = x : toList xs

{-
data LinkedList a =  [a] deriving (Eq, Show)

datum :: LinkedList a -> a
datum linkedList = head linkedList
--error "You need to implement this function."

fromList :: [a] -> LinkedList a
fromList xs = xs
--error "You need to implement this function."

isNil :: LinkedList a -> Bool
isNil linkedList = error "You need to implement this function."

new :: a -> LinkedList a -> LinkedList a
new x linkedList = error "You need to implement this function."

next :: error "You need to implement this function."

nil :: LinkedList a
nil = error "You need to implement this function."

reverseLinkedList :: LinkedList a -> LinkedList a
reverseLinkedList linkedList = error "You need to implement this function."

toList :: LinkedList a -> [a]
toList linkedList = error "You need to implement this function."

-}
