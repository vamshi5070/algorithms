import Data.Char

armstrong :: (Show a) => Integral a => a -> Bool
armstrong num = num == result
  where power = lengthShow num
        result = getArmStrong num power 0

lengthShow :: Show a => a -> Int
lengthShow num = length $ show num

-- getArmStrong :: Integral a => a -> Int -> a
getArmStrong :: (Integral t, Integral b) => t -> b -> t -> t
getArmStrong num power res
  | num < 0 = error "negative"
  | num < 10 = (num ^ power) + res
  | otherwise = getArmStrong (num `div` 10) power (((num `rem` 10) ^ power)+ res)
