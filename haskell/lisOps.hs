foldl''' :: (b -> a -> b) -> b -> [a] -> b
foldl''' f z [] = z
foldl''' f z (x:xs) = let z' = z `seq` f z x
                          in
                        foldl''' f z' xs


foldl'' :: (b -> a -> b) -> b -> [a] -> b
foldl'' f z [] = z
foldl'' f z (x:xs) = foldl'' f (f z x) xs

foldr' :: (a -> b -> b) -> b -> [a] -> b
foldr' f z [] = z
foldr' f z (x:xs) = (f x  (foldr' f z xs))

length' :: [a] -> Int
length' xs = sum [1 | x <- xs]

reverse' :: [a] -> [a]
reverse' xs = foldl' (\acc x -> x:acc) [] xs

map' :: (a -> b) -> [a] -> [b]
map' f [] = []
map' f (x:xs) = f x : map' f xs

filter' :: (a -> Bool) -> [a] -> [a]
filter' p [] = []
filter' p (x:xs)
  | p x = x : filter' p xs
  | otherwise = filter' p xs

append :: [a] -> [a] -> [a]
append xs ys = foldr (:) ys xs

concat' :: [[a]] -> [a]
concat' xss = foldr append [] xss

{-
foldl' :: (b -> a -> b) -> b -> [a] -> b
foldl' f z xs = error "You need to implement this function."

foldr :: (a -> b -> b) -> b -> [a] -> b
foldr f z xs = error "You need to implement this function."

length :: [a] -> Int
length xs = error "You need to implement this function."

reverse :: [a] -> [a]
reverse xs = error "You need to implement this function."

map :: (a -> b) -> [a] -> [b]
map f xs = error "You need to implement this function."

filter :: (a -> Bool) -> [a] -> [a]
filter p xs = error "You need to implement this function."

(++) :: [a] -> [a] -> [a]
xs ++ ys = error "You need to implement this function."

concat :: [[a]] -> [a]
concat xss = 
--error "You need to implement this function."
-}
