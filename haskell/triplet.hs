tripletsWithSum :: Int -> [(Int, Int, Int)]
tripletsWithSum total =  do
  x <- [1..total`div`2]
  y <- [x+1..total]
  let z = total - (x + y)
  if x^2 + y^2 == z^ 2
    then return (x,y,z)
    else []

-- [(x,y,z) | x <- [1..total],y <- [x+1..total], z <- total- x+y ,  x^2 + y ^ 2 == z ^ 2 ]
