checkSorted :: (Ord a) => [a] -> Bool
checkSorted [x] = True
checkSorted [x,y]
  | x <= y = True
  | otherwise = False
checkSorted (x:y:xs)
  | x <= y = checkSorted (y:xs)
  | otherwise = False
