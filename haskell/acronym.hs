import Data.Char

abbreviate :: String -> String
abbreviate xs = filter isAlpha $ foldr helper [] $ words xs

helper :: String -> [Char] -> [Char]
helper x acc
  | isAlpha (head x) && isAlpha (last x)  = 
  | all isUpper x || all (not . isUpper) x = toUpper (head x):acc
  | otherwise = (filter isUpper x) <> acc

-- extraClause = 
