import Data.Char
import Data.List

isogram :: String -> Bool
isogram str = let lowerStr = filter (\x -> x /= '-' && x /= ' ') $ map toLower str
                  in
                lowerStr ==  nub lowerStr

test = "six-year-old"
