data Tree a = Empty | Node  (Tree a) a (Tree a)
        deriving (Eq,Show)

oneTree = Node Empty 1 Empty
twoTree = Node (Node Empty 2 Empty) 3 Empty
threeTree = Node (Node Empty 4 Empty) 5 (Node Empty 6 Empty) 
actualTree = Node twoTree 1 threeTree

member :: (Ord a)  => a ->  Tree a -> Bool 
member key Empty = False
member key (Node l x r) 
        | key < x = member key l
        | key > x = member key r
        | otherwise = True

member' ::(Ord a) => a -> Tree a -> Bool
member' key Empty = False
-- member' key Node Empty x Empty = key == x
member' key t@(Node _ x _) =  aux x t
  where aux key' Empty = key == key'
        aux key' (Node l x r)
          | key < x = aux key' l
          | otherwise = aux x r

insert' :: (Ord a) => a -> Tree a -> Tree a
insert' key Empty = Node Empty key Empty
insert' key (Node l x r)
  | key < x = Node (insert' key l) x r
  | key > x = Node l x (insert' key r)
  | otherwise = error "Same value"

insert'' :: (Ord a) => a -> Tree a -> Tree a
insert'' key Empty = Node Empty key Empty
insert'' key t@(Node _ x _) = aux x t
  where aux key' Empty = if key' == key then error "same value" else Node Empty key Empty
        aux key'(Node l x r)
          | key < x  = Node (aux key' l) x r
          | otherwise = Node l x (aux x r)


complete :: (Ord a) => a -> Int -> Tree a
complete x 0 = Empty
complete x d = Node t x t
  where t = complete x (d-1)
-- (complete x (d-1)) x (complete x (d-1))


create :: (Ord a) => a -> Int -> Tree a
create x n
  | n == 0 = Empty
  | otherwise =  if (n - 1) `mod` 2 == 0 then
                   Node t x t
                 else Node l x r
  where t = create x ((n - 1) `div` 2)
        (l,r) = create2 x ((n-1) `div` 2)
        -- create2 :: (Ord a) => a -> Int -> (Tree a,Tree a)
        create2 x m = (create x m,create x (m+1))
