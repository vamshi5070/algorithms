import Data.Char

valid :: String -> Bool
valid str
  | length str <= 1 = False
  | readInt str == [] = False
  | result `rem` 10 == 0 = True
  | otherwise = False
    where result = helper (readInt str)

helper :: [Int] -> Int
-- helper [] = False
helper nums = fst $ foldr (\n (s,b) -> if b then (nineFlipper n + s,not b) else (n+s,not b)) (0,False) nums

readInt :: String -> [Int]
readInt num
  | any (not . isNumber) fNum = []
  | otherwise = map digitToInt fNum
  where fNum = filter (not . isSpace) num
-- readInt = map (read :: Char -> Int) filter (not . isSpace)

nineFlipper :: Int -> Int
nineFlipper num 
  | dNum > 9 = dNum - 9
  | otherwise = dNum
  where dNum  = num * 2
