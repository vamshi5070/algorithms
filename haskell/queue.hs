data Stack a =  Empty | Node a (Stack a)
  deriving (Eq,Show)

stack = Node 1 (Node 2 (Node 12 Empty))

data Queue a = EmptyQueue | Node_q a (Queue a)
  deriving (Eq,Show)

enqueue :: a -> Queue a ->  Queue a
enqueue x queue = Node_q x queue

dequeue :: Queue a -> Maybe a
dequeue EmptyQueue = Nothing
dequeue (Node_q x EmptyQueue) = Just x
dequeue (Node_q _ queue) = dequeue queue

listToQueue  xs = foldr (\x acc -> Node_q x acc) EmptyQueue xs 
