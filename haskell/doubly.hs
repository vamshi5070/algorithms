data Dll a = Empty | Node a (Dll a) (Dll a)
  deriving (Eq,Show)


example = Node 1 (Node 2) (Node 3)

data DLLNode a = DLLNode
  {value :: a
  , prev :: Maybe (DLLNode a)
  , next :: Maybe (DLLNode a)
  }

data DoblyLinkedList a = DoblyLinkedList
  { headDLL :: Maybe (DLLNode a)
  , tailDLL :: Maybe (DLLNode a)
  }

insertFront :: a -> DoblyLinkedList a -> DoblyLinkedList a
insertFront val list =
  let newNode = DLLNode val Nothing (headDLL list)
  in case headDLL list of
    (Just node) -> list {headDLL = Just newNode,tailDLL = tailDLL list}
    Nothing -> list {headDLL = Just newNode, tail = Just newNode}

removeFront :: DoblyLinkedList a -> Maybe (DoblyLinkedList a)
removeFront list = case head list of
  Just node -> 
