fn main() {
    println!("enter here");
    score("cabbage");
}

fn score(word: &str) -> u64 {
    let lowercased : String = word.to_lowercase();
    let list : Vec<u64> = lowercased.chars().map(|c| my_hash_map(c)).collect();
    let total : u64 = list.iter().sum();
    total
}

fn my_hash_map(character :char) -> u64 {
    match character {
	'a' | 'e' | 'i' | 'o' | 'u' | 'l' | 'n' | 'r' | 's' | 't' => 1,
	'd' | 'g' => 2,
	'b' | 'c' | 'm' | 'p' => 3,
	'f' | 'h' | 'v' | 'w' | 'y' => 4,
	'k' => 5,
	'j' | 'x' => 8,
	'q' | 'z' => 10,
	_ => 0
    }
}
