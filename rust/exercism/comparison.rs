fn main() {
    let array2 = [1,2,4];
    let array = [1,2,3,4,5];
    println!("main of terminal oriented editor");
    println!("equal {:?}",sublist(&array,&array2));
    
}

#[derive(Debug)]
pub enum Comparison {
    Equal,
    Sublist,
    Superlist,
    Unequal,
}

fn sublist <T: PartialEq>(first_list: &[T], second_list: &[T]) -> Comparison {
    if first_list.len() == second_list.len() {
	if first_list == second_list {
	    return Comparison::Equal;
	} else {
	    return Comparison::Unequal;
	}
    } else if first_list.len() < second_list.len(){
	for element in second_list.iter() {
	    if element == first_list[0] {
		for ()
	    }
	}
    }
}


fn sublist_unordered <T: PartialEq>(first_list: &[T], second_list: &[T]) -> Comparison {
    if first_list.len() == second_list.len() {
	if first_list == second_list {
	    return Comparison::Equal;
	} else {
	    return Comparison::Unequal;
	}
    } else if first_list.len() < second_list.len(){
	let mut part_of : bool = true;
	for element in first_list.iter() {
	    let contains : bool = second_list.contains(&element);
	    part_of = part_of && contains;
	    if part_of {
		continue;
	    } else {
		return Comparison::Unequal;
		// break;
	    }
	}
	return Comparison::Sublist;
    }
    else if first_list.len() > second_list.len(){
	let mut part_of : bool = true;
	for element in second_list.iter() {
	    let contains : bool = first_list.contains(&element);
	    part_of = part_of && contains;
	    if part_of {
		continue;
	    } else {
		return Comparison::Unequal;
		// break;
	    }
	}
	return Comparison::Superlist;
    }
    else {
	return Comparison::Unequal;
    }
}
