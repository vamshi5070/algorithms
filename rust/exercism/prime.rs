fn main(){
    println!("main");
    let input: u32 = 1;
    println!("{}th {}",input,nth(input));
}

fn nth (n: u32) -> u32 {
    let mut cnt : u32 = n+1;
    let mut cur_num: u32 = 2;
    let mut recent_prime : u32 = 2;
    loop {
	if cnt <= 0 {
	    break;
	} else if prime(cur_num) {
	    cnt = cnt - 1;
	    recent_prime = cur_num;
	    cur_num = cur_num + 1;
	} else {
	    cur_num = cur_num + 1;
	}
    }
    recent_prime 
}

fn prime(number : u32) -> bool {
    if number <=  1{
	return false
    } else {
	let n_sqrt = (number as f64).sqrt() as u32;
	for divisor in 2..=n_sqrt {
	    if number % divisor == 0 {
		return false;
	    }
	}
    }
    true
}


    // for dividend in 2.. {
    // 	if prime(dividend) {
    // 	    cnt = cnt - 1;
    // 	} else {
    // 	    continue;
    // 	}
    // }
