fn main(){
    let input = "\\left(\\begin{array}{cc} \\frac{1}{3} & x\\\\ \\mathrm{e}^{x} &... x^2 \
                 \\end{array}\\right)";
    println!("truth is, {}",brackets_are_balanced(input));
    println!("truth is, {}",brackets_are_balanced("{}["));
    println!("truth is, {}",brackets_are_balanced("[({]})"));
    println!("truth is, {}",brackets_are_balanced("[]{}"));
	//"([{}({}[])])"));
// {}[]"));
}

pub fn brackets_are_balanced(string: &str) -> bool{
    let mut stack : Vec<char> = Vec::new();
    for character in string.chars() {
	if stack.is_empty() {
	    match character {
		'[' | '{' | '(' => {
		    stack.push(character);
		}
		']' | '}' | ')' => {
		    return false;
		}
		_ => continue,
	    }
	} else {
	    match character {
		'[' | '{' | '(' => {
		    stack.push(character);
		}
		']' => {
		    let head = stack.pop().unwrap();
		    // println!("{}",temp);
		    if head == '[' {
			continue;
		    } else {
			return false;
		    }
		}
		'}' => {
		    let head = stack.pop().unwrap();
		    // println!("{}",temp);
		    if head == '{' {
			continue;
		    } else {
			return false;
		    }
		}
		')' => {
		    let head = stack.pop().unwrap();
		    // println!("{}",temp);
		    if head == '(' {
			continue;
		    } else {
			return false;
		    }
		}
		_ => continue,
	    }
	}
    }
    stack.is_empty()
}

pub fn multiple_brackets_are_balanced(string: &str) -> bool{
    let mut stack_square: Vec<char> = Vec::new();
    let mut stack_flower: Vec<char> = Vec::new();
    let mut stack_bracket: Vec<char> = Vec::new();

    // true means closed
    let mut pred_square : bool = true;
    let mut pred_flower : bool = true;
    let mut pred_bracket : bool = true;
    
    for character in string.chars() {
	match character {
	    '[' => {
		stack_square.push(character);
		pred_square = false;
	    },
	    '{' => {
		stack_flower.push(character);
		pred_flower = false;
	    },
	    '(' => {
		stack_bracket.push(character);
		pred_bracket = false;
	    },
	    ']' => if !pred_square && stack_square.is_empty() || !pred_flower || !pred_bracket {
		return false;
	    } else {
		stack_square.pop();
		pred_square = !pred_square;
	    }
	    '}' => if !pred_flower && stack_flower.is_empty() || !pred_square || !pred_bracket {
		return false;
	    } else {
		pred_flower = !pred_flower;
		stack_flower.pop();
	    }
	    ')' => if !pred_bracket && stack_bracket.is_empty() || !pred_flower || !pred_square {
		return false;
	    } else {
		pred_bracket = !pred_bracket;
		stack_bracket.pop();
	    }
	    _ => continue,
	}
    }
    stack_bracket.is_empty() && stack_flower.is_empty() && stack_square.is_empty()
    // return true;
}
