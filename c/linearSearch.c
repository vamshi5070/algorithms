#include <stdio.h>

int linearSearch(int* arr,int size,int target){
  int i;
  for (i = 0; i < size; i++){
    if (target == arr[i]){
      return i;
    } else {
      continue;
    }
  }
  return -1;
}

int main() {
  int array[10] = {5,3,12,4,54,123,99};
  int res = linearSearch(array,10,9900);
  if (res == -1) {
      printf("not found in array\n");
    } else {
    printf("res is %d\n", res);
  }
}
  
