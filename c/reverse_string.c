#include<stdio.h>
#include<stdlib.h>

int vam_strlen(const char *str){
  int cnt = 0,i= 0;
  while (str[i] != '\0'){
    cnt = cnt + 1;
    i = i + 1;
  }
  return cnt;
}

char *reverse(const char *value){
  int i=0,len_str=vam_strlen(value);
  int j = 0;
  char * rs;
  rs = malloc(len_str+1);
  for(i = len_str-1;i >= 0 ; i--){
    rs[j] = value[i];
    j = j + 1;
  }
  rs[len_str] = '\0';
  return rs;
}

int main(){
  /* printf("% " ,reverse("emacs")); */
  printf("%s",reverse("worst"));
  return 0;
}
