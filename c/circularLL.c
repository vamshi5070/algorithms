#include <stdio.h>
#include <stdlib.h>

struct Node {
  int data;
  /* struct Node* prev; */
  struct Node* next;
};

struct Node* createNode(int data){
  struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
  newNode -> data = data;
  newNode -> next = NULL;
  /* newNode -> prev = NULL; */
  return newNode;
}

struct Node* arrayToListC(int* arr,int size){
  printf("Print list:  \n");
  struct Node* head=NULL;
  struct Node* first=NULL;
  struct Node* pointer=NULL;
  struct Node* temp=NULL;
  if (size < 1) {
    printf("empty list!! \n");
  } else {
    head = createNode(arr[0]);
    head -> data = 1;
    pointer = head;
    first = head;
    pointer -> next = first;
    for (int i = 1; i < size;i++){
      temp = createNode(arr[i]);
      temp -> next = first;
      pointer -> next = temp;
      pointer = temp;
    }
  }
  return head;
  /* return NULL; */
}


void printListCircular(struct Node* node){
  /* cond = node -> data; */
  int headData = node -> data;
  printf("%d \n", headData);
  node = node -> next;
  while(node != NULL && headData != node -> data){
    printf("%d \n", node -> data);
    node = node -> next;
  }
  printf("ended.... \n");
}

int main(){
  int arr[5] = {1,2,3,4,5};
  int size = 5;
  struct Node* head=NULL;
  printf("Print list:  \n");
  head = arrayToListC(arr,size);
  printListCircular(head);
}
