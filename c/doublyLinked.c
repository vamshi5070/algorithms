#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

struct Node {
  int data;
  struct Node* prev;
  struct Node* next;
};

struct Node* createNode(int data){
  struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
  newNode -> data = data;
  newNode -> next = NULL;
  newNode -> prev = NULL;
  return newNode;
}

struct Node* arrayToList(int arr[],int size,struct Node** last){
  struct Node* head=NULL;
  struct Node* pointer=NULL;
  struct Node* temp=NULL;

  if (size < 1) {
    printf("empty list!! \n");
  } else {
    head = createNode(arr[0]);
    pointer = head;

    for (int i = 1; i < size;i++){
      temp = createNode(arr[i]);
      pointer -> next = temp;
      temp -> prev = pointer;
      pointer = temp;
    }
  }
  (*last) = pointer;
  return head;
}

void printListPrev(struct Node* node){
  printf("Previous numbers, \n");
  struct Node* temp = node;
  while(temp != NULL){
    printf("%d ", temp -> data);
    temp = temp -> prev;
  }
  printf("ended.... \n");
}
struct Node* arrayToLinkedList(int arr[], int size, struct Node** last) {
  struct Node* head = NULL;
  struct Node* pointer = NULL;
  struct Node* temp = NULL;
  
  if (size < 1) {
    printf("empty list!! \n");
  } else {
    head = createNode(arr[0]);
    pointer = head;
    
    for (int i = 1; i < size; i++) {
      temp = createNode(arr[i]);
      (pointer) -> next = temp;
      temp -> prev = pointer;
      pointer = temp;
    }
  }
  (*last) = pointer;
  return head;
}

void printListNext(struct Node* node){
  printf("Next numbers, \n");
  struct Node* temp = node;
  while(temp != NULL){
    printf("%d ", temp -> data);
    temp = temp -> next;
  }
  printf("ended.... \n");
}

int main(int argc, char *argv[]){
  struct Node* tail = NULL;
  struct Node* head = NULL;
  int arr[5] = {1,2,3,4,5};
  int size = sizeof(arr) / sizeof(arr[0]);
  int i;
  bool a2ListPred = false;
  printf("Doubly linked list \n");
  printf("------------------ mine -----------------------------------\n");
  head = arrayToLinkedList(arr, size, &tail);
  printListPrev(tail -> prev);
  printListNext(head);
  /* printf("%d \n",last -> data); */
  printf("------------------ yours -----------------------------------\n");

  printf("Doubly linked list \n");
  head = arrayToList(arr,size,&tail);

    // Print the doubly linked list
  printListPrev(tail);
  printf("Doubly Linked List: ");
  printListNext(head);
}
/*

        head = NULL;
        *last = NULL;
        return;
*/
