#include "stdio.h"

void printArray(int* arr,int size){
  int i;

  for (i = 0; i < size; i++){
    printf("%d ",arr[i]);
  }
  printf("\n");
}

// merges two sorted arrays
void merge(int* arr,int* arr1,int* arr2, int size1,int size2){
  int temp,i=0,j=0,k=0;
  while(i < size1 && j < size2) {
    if (arr1[i] < arr2[j]){
      arr[k] = arr1[i];
      i = i + 1;
      /* printf("x: %d \n",arr1[i]); */
    } else {
      arr[k] = arr2[j];
      j = j + 1;
      /* printf("y: %d \n",arr2[j]); */
    }
    k = k + 1;
    /* printf("size what ? %d \n",k); */
  }
  while (i < size1){
    arr[k] = arr1[i];
    i = i + 1;
    k = k + 1;
  }
  while (j < size2){
    arr[k] = arr2[j];
    j = j + 1;
    k = k + 1;
  }
  /* return arr; */
}

void mergeSort(int* arr,int size){
  if (size < 2){
    return ;
  } else {
    int mid = size / 2;
    /* printf("%d \n",mid); */
    int leftArr[5],j=0;
    int rightArr[5],k=0;
    for (int i = 0; i < mid; i++){
      leftArr[j] = arr[i];
      j = j + 1;
    }
    for (int i = mid; i < size; i++){
      rightArr[k] = arr[i];
      k = k + 1;
    }
    mergeSort(leftArr,mid);
    mergeSort(rightArr,size-mid);
    
    merge(arr,leftArr,rightArr,mid,size-mid);
  }
}

int main(){
  int arr[5] = {5,4,3,2,1};
  int size = 5;
  mergeSort(arr,5);
  printArray(arr,size);
  printf("%d \n", (0 + 1)/2);
  int leftArr[5] = {4};
  int rightArr[5] = {1};
}
