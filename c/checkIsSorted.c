#include <stdio.h>
#include <stdbool.h>

bool checkSorted(int* arr, int size){
  int i;
  
  for (i = 0; i < size-1; i++){
    if (arr[i] > arr[i+1]){
      return false;
    } else {
      continue;
    }
  }
  return true;
}

void printArray(int* arr,int size){
  int i;

  for (i = 0; i < size; i++){
    printf("%d ",arr[i]);
  }
  printf("\n");
}

void swap(int* arr,int index1,int index2){
  int temp = arr[index2];
  arr[index2] = arr[index1];
  arr[index1] = temp;
}

void bubbleSort(int* arr,int size){
  int i;
  int j;
  for (i = 0; i < size; i++){
    for (j = i+1; j < size; j++){
      if (arr[i] > arr[j]) {
	swap(arr,i,j);
      } else {
	continue;
      }
    }
  }
}

void insertionSort(int* arr,int size){
  int i;
  int j;
  int key;
  for (i = 1; i < size; i++){
    j = i-1;
    key = i;
    for (j = i-1; j >= 0; j--){
      if (arr[i] < arr[j]){
	swap(arr,i,j);
	i = j;
      } else {
	break;
      }
    }
  }
}

void selectionSort(int* arr,int size){
  int i,j,min;
  for (i = 0; i < size-1; i++){
    min = i;
    for (j = i+1; j < size; j++){
      if (arr[min] > arr[j]){
	min = j;
      } else {
	continue;
      }
    }
    swap(arr,i,min);
  }
}

void merge(int* arr1 , int* arr2,int size1,int size2,int* mergeArr){
  int temp,i=0,j=0,k=0;
  while(i < size1 && j < size2) {
    if (arr1[i] < arr2[j]){
      mergeArr[k] = arr1[i];
      i = i + 1;
      printf("x: %d \n",arr1[i]);
    } else {
      mergeArr[k] = arr2[j];
      j = j + 1;
      printf("y: %d \n",arr2[j]);
    }
    k = k + 1;
    printf("size what ? %d \n",k);
  }
  while (i < size1){
    mergeArr[k] = arr1[i];
    i = i + 1;
  }
  while (j < size2){
    mergeArr[k] = arr2[j];
    j = j + 1;
  }
}

/* void mergeSort(int* arr,int size){ */
/*   int i,j,min; */
/*   int low = 0; */
/*   int high = size; */
/*   while(low <= high){ */
/*     int mid = (low + (high-1))/2; */
/*     arr[] */
/*   } */
/* } */
