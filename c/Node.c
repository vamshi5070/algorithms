// private
#include "stdlib.h"

struct Node_int{
  int data;
  struct Node_int * next;
};

struct Node_int* createNode(int data){
  struct Node_int* newNode = malloc(sizeof(struct Node_int));
  newNode -> data = data;
  newNode -> next = NULL;
  return newNode;
}

