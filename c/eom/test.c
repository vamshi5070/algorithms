#include "DataStructures/LinkedList.h"
#include <stdio.h>
#include <stdlib.h>

int main(){
  /* printf("%d \n",rgv); */

  struct LinkedList list = linked_list_init();
  int arr[5] = {5,4,3,2,1};
  int size = 5;

  printf("Inserting..... \n");
  for(int i=0; i < size; i++){
    int *x = (int *)malloc(sizeof(int));
    *x = arr[i];
    insert_node(i,x,&list);
  }
  printf("Printing............... \n");
  for(int i = 0; i < size;i++){
    int* temp = retrieve_data(i,&list);
    printf("%d ",*temp);
  }
  
}
