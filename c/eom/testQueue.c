#include <stdio.h>
#include <stdlib.h>
#include "DataStructures/Queue.h"

int main(){
  struct Queue queue = queue_init();
  int *x = (int *)malloc(sizeof(int));
  *x = 2;
  printf("Pushing 2 into queue... \n");
  push(x,&queue);
  int *y = (int *)malloc(sizeof(int));
  *y = 3;
  printf("Pushing 3 into queue... \n");
  push(y,&queue);
  int *z = (int *)malloc(sizeof(int));
  printf("Pushing 4 into queue... \n");
  *z = 4;
  push(z,&queue);
  printf("Printing............... \n");
  /* int* temp = pop(&queue); */
  /* printf("%d ",*temp); */
  int size = 3;
  printf("Printing............... \n");
  for(int i = 0; i < size;i++){
    int* temp = retrieve_data(i,&queue.list);
    printf("%d ",*temp);
  }
  /* int* temp = retrieve_data(0,&queue.list); */
  /* printf("%d ",*temp); */
  /* printf("%d ",queue.list.length); */
  printf("Printing");
}
