#ifndef LinkedList_h
#define LinkedList_h

#include "Node.h"
#include <stdlib.h>
#include <stdio.h>

struct LinkedList {
  struct Node* head;
  int length;

};
struct LinkedList linked_list_init();
void insert_node (int index,void *data, struct LinkedList *linked_list);
void remove_node (int index,struct LinkedList *linked_list);
void* (retrieve_data)(int index,struct LinkedList *linked_list);
/* int rgv = 3; */
#endif
