#ifndef Node_h
#define Node_h

struct Node{
  int *data;
  struct Node * next;
};
struct Node* create_node_int(int data);
#endif
