#include "Queue.h"
#include "string.h"
#include "stdlib.h"

struct Queue queue_init(){
  struct Queue queue;
  queue.list = linked_list_init();
  return queue;
}

void push(void* data,struct Queue* queue){
  int index = queue->list.length;
  /* printf("index: \n"); */
  /* printf("%d \n",index); */
  insert_node(index,data,&queue->list);
}

void* pop(struct Queue *queue){
  void* item = retrieve_data(0,&queue->list);
  void* itemCpy = malloc(sizeof(*item));
  memcpy(itemCpy,item,sizeof(*item));
  remove_node(0,&queue->list);

  printf("1 \n");
  int* temp = itemCpy;
  printf("Deleting \n");
  printf("%d \n",*temp);
  return itemCpy;
}

