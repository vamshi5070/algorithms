#include <stdio.h>
#include <stdlib.h>

// Function pointer type for the transformation function
typedef int (*TransformFunc)(int);

// Higher-order function: map
int* map(int* arr, size_t size, TransformFunc transform) {
    int* result = malloc(size * sizeof(int));

    for (size_t i = 0; i < size; i++) {
        result[i] = transform(arr[i]);
    }

    return result;
}

// Example transformation functions
int square(int x) {
    return x * x;
}

int increment(int x) {
    return x + 1;
}

int main() {
    int original[] = {1, 2, 3, 4, 5};
    size_t size = sizeof(original) / sizeof(original[0]);

    int* squared = map(original, size, square);
    int* incremented = map(original, size, increment);

    printf("Original: ");
    for (size_t i = 0; i < size; i++) {
        printf("%d ", original[i]);
    }
    printf("\n");

    printf("Squared: ");
    for (size_t i = 0; i < size; i++) {
        printf("%d ", squared[i]);
    }
    printf("\n");

    printf("Incremented: ");
    for (size_t i = 0; i < size; i++) {
        printf("%d ", incremented[i]);
    }
    printf("\n");

    free(squared);
    free(incremented);

    return 0;
}
