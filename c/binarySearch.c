#include "checkIsSorted.c"

void printBool(int* arr,int size){
  bool pred = checkSorted(arr,size);
  if(pred){
    printf("sorted arr \n");
  } else {
    printf("unsorted arr \n");
  }
}

/* binary search using while loop */
int * binarySearch(int* arr,int size,int target){
  int mid=-1;
  int low = 0;
  int high = size;
  while(low <= high){
    mid = (low + (high - 1)) / 2;
    if (arr[mid] == target){
      return &mid;
    } else if(target < arr[mid]){
      high = mid - 1;
      continue;
    } else {
      low = mid + 1;
      continue;
    }
  }
  return &mid;
}

/* recursive binary search */
int recBinarySearch(int* arr,int low,int high,int target){
  int mid = (low + (high - 1)) / 2;
  if (high < low) {
      return -1;
  } else {
    if (arr[mid] == target){
      return mid;
    } else if (target < arr[mid]) {
      recBinarySearch(arr,low,mid-1,target);
    } else {
      recBinarySearch(arr,mid+1,high,target);
    }
  }
}

int main(){
  int arr[10] = {1,2,3,4,5,6,7,8,9,10};
  int size = 10;
  int pred,index;
  printBool(arr,size);
  pred = checkSorted(arr,size);
  if(!pred){
    printf("Give a sorted array \n");
  } else {
    index = *binarySearch(arr,size,1);
    if (index == -1) {
      printf("error, target not found \n");
    } else {
      printf("Target found on %d \n",index);
    }
  }
}
