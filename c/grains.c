#include <math.h>
#include <stdint.h>
#include <stdio.h>

uint64_t square(uint8_t index){
  if (index <= 0 || index >= 65){
    return 0;
  }
  return pow(2,index-1);
}

uint64_t total(void) {
  return ((square(64) - 1) + square(64));
}
int main(){
  printf("%ld \n",square(64));
  printf("%ld \n",total());
  return 0;
}
