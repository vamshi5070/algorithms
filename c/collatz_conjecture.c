#include<stdio.h>

#define ERROR_VALUE -1

int steps(int start){
  int i =0, cnt = 0;
  int num = start;
  if (num <= 1){
    return -1;
  }
  while (num != 1){
    if(num % 2 == 0){
      num = num / 2;
    } else {
      num = 3 * num + 1;
    }
    cnt = cnt + 1;
  }
  return cnt;
}

int main(){
  printf("%d " ,steps(-15));
  return 0;
}
