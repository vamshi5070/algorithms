#include "checkIsSorted.c"

int main(){
  int arr[10] = {10,12,31,4,75,6,117,8,99,10};
  /* int arr[5] = {3,1,2,4,5}; */
  int sizeRev = 5;
  int arrRev[5] = {5,4,3,2,1};
  int size = 10;
  printf("Insertion sort: \n");
  insertionSort(arr,size);
  printArray(arr,size);
  printf("Insertion sort rev: \n");
  insertionSort(arrRev,sizeRev);
  printArray(arrRev,sizeRev);

  int arr1[5] = {1,3,5,7,9};
  int arr2[5] = {2,4,6,8,10};
  int size1 = 5;
  int size2 = 5;
  int mergeArray[30];
  int sizeMerge = size1+size2;
  printf("after merge \n");
  merge(arr1,arr2,size1,size2,mergeArray);
  printArray(mergeArray,sizeMerge);
}

/*
  int array[10] = {10,12,31,4,75,6,117,8,99,10};
  selectionSort(array,size);
  printArray(array,size);
*/
