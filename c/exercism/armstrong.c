#include <stdio.h>
/* #include <math.h> */

// myPow is custom math function
int myPow(int base, int power){
  int i=0;
  int product = 1;
  for (i = 0;i < power; i++){
    product = product * base;
  }
  return product;
}

int digitsInOrder (int num,int* arr,int size) {
  int i = size - 1;
  while (num != 0 && i >= 0) {
    arr[i] = num % 10;
    num = num / 10;
    i = i - 1;
  }
  return i+1;
}
int digitsNotInOrder (int num,int* arr,int size) {
  int i = 0;
  while (num != 0 && i < size) {
    arr[i] = num % 10;
    num = num / 10;
    i = i - 1;
  }
  return i+1;
}

int armStrong (int start,int end, int* arr) {
  int i = 0;
  int sum = 0;
  int power = end - start;
  for (i = start; i < end; i++){
    sum = sum + myPow(arr[i],power);
  }
  return sum;
}

void printArray(int* arr,int start,int end){
  int i;
  printf("Printing array...\n");
  for (i = start; i < end; i++){
    printf("%d = %d\n",i, arr[i]);
  }
}

int main() {
  int nine = 9;
  int ten = 10;
  int arr[5];
  int size=5;
  int retIndex;
  int oneFiveThree = 153;
  retIndex = digitsInOrder(oneFiveThree,arr,size);
  printf("Armstrong %d = %d\n",oneFiveThree,armStrong(retIndex,size, arr));
  int power = size - retIndex;
  printf("%d\n",oneFiveThree);
  printf("%d\n", oneFiveThree % 10);

  /* printf("Max digits: \n",MAX_DIGITS); */
}

    /* printf("num: "); */
    /* printf("%d\n",num); */
    /* printf("%d\n",arr[i]); */
  /* return arr; */
    /* printf("num: "); */
    /* printf("%d\n",num); */
    /* printf("%d\n",arr[i]); */
  /* printf("return value %d",i); */
  /* printf("return value %d",i+1); */
  /* return arr; */
  /* int temp; */
  /* printf("Armstrong %"); */
  /* printf("Power:- %d = %d\n",ten, myPow(ten,0)); */
  /* printArray(arr,retIndex,size); */
  /* printf("%d is armstrong\n ", nine, arr); */
