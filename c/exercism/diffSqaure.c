#include <stdio.h>

// 1^2 + 2^2 + 3^2
unsigned int sum_of_squares(unsigned int number){
  int sum = 0;
  for(int i=1;i<=number;i++ ){
    sum  = sum + i * i;
  }
  return sum;
}

// (1 + 2 + 3) ^ 2
unsigned int square_of_sum(unsigned int number){
  int sum = 0;
  for(int i=1;i<=number;i++ ){
    sum  = sum + i ;
  }
  return sum*sum;
}

// (1 + 2 + 3)^2 - 1^2 + 2^2 + 3^2
unsigned int difference_of_squares(unsigned int number){
  return square_of_sum(number)- sum_of_squares(number);
}


int main(){
  printf("sum_of_squares(%d) = %d \n",10,sum_of_squares(10));
  printf("square_of_sum(%d) = %d \n",10,square_of_sum(10));
  printf("difference_of_squares(%d) = %d \n",10,difference_of_squares(10));
}
