#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

struct Node {
  int data;
  struct Node* next;
};

struct Node* createNode(int data){
  struct Node* newNode = malloc(sizeof(struct Node));
  newNode -> data = data;
  newNode -> next = NULL;
  return newNode;
}

void printList(struct Node* node){
  while(node != NULL){
    printf("%d ", node -> data);
    node = node -> next;
  }
  printf("ended.... \n");
}

void printListRecursive(struct Node* node){
  if(node == NULL){
    printf("ended.... \n");
    return;
  } else {
    printf("%d \n", node -> data);
    printListRecursive(node -> next);
  }
}

struct Node* arrayToList(int* arr,int size,bool* a2ListPred){
  int i;
  struct Node* pointer;
  struct Node* head;
  struct Node* temp;
  if (size < 1) {
    printf("empty list!! \n");
  } else {
    head = createNode(arr[0]);
    pointer = head;
    /* a2ListPred = true; */
    for (i = 1; i < size;i++){
      temp = createNode(arr[i]);
      pointer -> next = temp;
      pointer = temp;
	}
  }
  return head;
}

struct Node* insertAfter(struct Node* head,int num,int before){
  struct Node* prev = NULL;
  struct Node* current = head;
  struct Node* temp = NULL;
  while (current != NULL) {
    if(current -> data == before){
      temp = createNode(num);
      temp -> next = current -> next;
      current -> next = temp;
      return head;
    } else {
      printf("%d \n", current -> data);
      current  = current -> next  ;
    }
  }
  return head;
}

struct Node* insertBefore(struct Node* head,int num,int after){
  struct Node* prev = NULL;
  struct Node* current = head;
  struct Node* temp = NULL;
  while (current != NULL) {
    if(current -> next != NULL && (current -> next) -> data == after){
      temp = createNode(num);
      temp -> next = current -> next;
      current -> next = temp;
      return head;
    } else {
      printf("%d \n", current -> data);
      current  = current -> next  ;
    }
  }
  return head;
  
}

// cons
struct Node* insert(struct Node* head,int num){
  struct Node* prev = NULL;
  struct Node* current = head;
  struct Node* temp = NULL;
  temp = createNode(num);
  temp -> next = head;
  return temp;
}

struct Node* delete(struct Node* head,int num){
  struct Node* prev = NULL;
  struct Node* current = head;
  struct Node* temp = NULL;
  if (head == NULL){
    printf("Empty list\n");
  } else if (current -> data == num) {
    head = current -> next;
  } else {
    while (current -> next != NULL) {
      if((current -> next) -> data == num ){
	current -> next = current -> next -> next;
	return head;
      } else {
	current  = current -> next  ;
      }
    }
  }
  return head;
}

struct Node* reverseLinkedList(struct Node* head){
  struct Node* prev = NULL;
  struct Node* current = NULL;
  struct Node* temp = head;
  printf("Reverse \n");
  while (temp != NULL) {
    current = temp;
    temp = temp -> next;
    current -> next = prev;
    prev = current;
  }
  return head;
}


bool search(struct Node* head,int num){
  struct Node* prev = NULL;
  struct Node* current = head;
  struct Node* temp = head;
  while(current != NULL){
    if (current -> data == num){
      return true;
    }
      current = current -> next;
  }
  return false;

}
