#include <stdio.h>
#include <string.h>

int vam_strlen(const char *str){
  int cnt = 0,i= 0;
  while (str[i] != '\0'){
    cnt = cnt + 1;
    i = i + 1;
  }
  return cnt;
}

int compute(const char *str1, const char *str2){
  int i=0,j = 0,cnt=0;
  if (vam_strlen(str1) == vam_strlen(str2)){
    while(str1[i] != '\0' && str2[j] != '\0'){
      if (str1[i] != str2[j]) {
	cnt = cnt + 1;
      } 
      i= i+1;
      j= j+1;
    }
  } else {
    cnt = -1;
  }
  return cnt;
}

int main(){
  printf("printing through iteration \n");
  char str1[] = "A";
  char str2[] = "A";
  printf("%d \n",compute(str1,str2));
}
