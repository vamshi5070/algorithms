#include <stdio.h>

int main() {
  int size = 3;
  int list[5] = {1,2,3};

  printf("Before increment\n");
  for (int i = 0; i < size; i++){
    printf("i = %d\n",list[i]);
  }

  for (int i = 0; i < size; i++){
    list[i] = list[i] + 1;
    /* printf("i = %d\n",i); */
  }
  
  printf("After increment\n");
  for (int i = 0; i < size; i++){
    printf("i = %d\n",list[i]);
  }
  /* printf("hello world\n"); */
}
