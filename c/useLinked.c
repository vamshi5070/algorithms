#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "linkedList.c"

void swapNodes(struct Node* node1,struct Node* node2){
  int tempData;
  tempData = node1 -> data;
  node1 -> data = node2 -> data;
  node2 -> data = tempData;
}



struct Node* bubbleSort(struct Node* head){
  struct Node* prev = NULL;
  struct Node* current = head;
  struct Node* current2 = head;
  struct Node* temp = NULL;
  int tempData;
  while(current != NULL){
    current2 = current -> next;
    while (current2 != NULL){
      if ((current -> data) > current2 -> data){
	swapNodes(current,current2);
      } else {
	current2  = current2 -> next;
      }
    }
    current = current -> next;
  }
  return head;
}

struct Node* insertionSort(struct Node* head){
  struct Node* prev = NULL;
  struct Node* current = head -> next;
  struct Node* current2 = head;
  struct Node* temp = NULL;
  struct Node* fresh = NULL;
  int tempData;
  while(current != NULL){
    current2 = head;
    while (current2 != current){
      if ((current2 -> data) > current -> data){
	swapNodes(current,current2);
      } else {
	current2  = current2 -> next;
      }
    }
    current = current -> next;
  }
  return head;

}


struct Node* selectionSort(struct Node* head){
  struct Node* prev = NULL;
  struct Node* current = head;
  struct Node* current2 = head;
  struct Node* temp = NULL;
  struct Node* min = NULL;

  int tempData;
  /* int min; */
  while(current != NULL){
    min  = current; // -> data = current -> data;
    current2 = current -> next;
    
    while (current2 != NULL){
      if ((min -> data) > current2 -> data){
	min = current2;
      } else {
      }
      current2  = current2 -> next;
    }
    swapNodes(min,current);
    current = current -> next;
  }
  return head;
}
/*
	tempData = current -> data;
	current -> data = current2 -> data;
	current2 -> data = tempData;

*/

int main(int argc, char *argv[]){
  int arr[5] = {1,2,3,4,5};
  int size = 5;
  int i;
  bool a2ListPred = true;
  struct Node* oneToFive;
  printf("linked list \n");
  oneToFive = arrayToList(arr,size,&a2ListPred);
  if (a2ListPred) {
    printList(oneToFive);
  } else {
    printf("no array to linked list \n");
  }
  struct Node* first = createNode(3);
  struct Node* snd = createNode(12);
  struct Node* thrd = createNode(30);
  /* printList(oneToFive); */
  printf("Nullified.... \n");

  struct Node* nullified=insertAfter(NULL,10,3);
  printList(nullified);
  struct Node* insertHeadBefore = insertBefore(oneToFive,10,3);
  printList(insertHeadBefore);
  printf("Ackshually.... \n");
  printList(oneToFive);
  
  first -> next = snd;
  /* snd -> next  = thrd; */
  printListRecursive(first);
  printf("cons \n");
  struct Node* cons = insert(NULL,10);
  printListRecursive(cons);

  printf("Before : \n");
  printList(oneToFive);
  printf("Deletion: \n");
  struct Node* deleted = delete(oneToFive,5);
  printList(deleted);
  free (first);
  free (snd);
  free (thrd);

  int arRev[5] = {5,4,3,2,1};
  int sizeRev = 5;
  bool a2RevBool = false;
  printf("Bubble sort \n");
  printf("Before: \n");
  struct Node* rev = arrayToList(arRev,sizeRev,&a2RevBool);
  printList(rev);
  struct Node* resBubble =  bubbleSort(rev);
  printList(resBubble);

  int arInsRev[5] = {5,4,3,2,1};
  printf("Insertion sort: \n");
  printf("Before: ");
  struct Node* insRev = arrayToList(arRev,sizeRev,&a2RevBool);
  printList(insRev);
  struct Node* resIns =  insertionSort(insRev);
  printList(resIns);

  /* int arSelRev[5] = {5,4,3,2,1}; */
  printf("Selection sort: \n");
  printf("Before: ");
  struct Node* selRev = arrayToList(arRev,sizeRev,&a2RevBool);
  printList(selRev);
  struct Node* resSel =  selectionSort(selRev);
  printList(resSel);

  if (search(resSel,13)) {
    printf("found \n");
  } else {
    printf("not found \n");
  }

}
