#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>

/*** defines ***/
#define CTRL_KEY(k) ((k) && 0x1f)

/*** data ***/
struct termios orig_termios;

/*** terminal ***/
void die(const char *s){
  perror(s);
  exit(1);
}

void disableRawMode() {
  if (tcsetattr(STDIN_FILENO , TCSAFLUSH, &orig_termios) == -1){
    die("tcsetattr");
  }
}

void enableRawMode() {
  if (tcgetattr(STDIN_FILENO , &orig_termios) == -1) die("tcgetattr");
  atexit(disableRawMode);
  
  struct termios raw = orig_termios;

  /* raw.c_iflag = raw.c_iflag & ~(ICRNL | IXON); */
  raw.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON); 
  raw.c_oflag = raw.c_oflag & ~(OPOST);
  raw.c_cflag = raw.c_cflag & ~(CS8);
  raw.c_lflag = raw.c_lflag & ~(ECHO | ICANON  | IEXTEN | ISIG);
  raw.c_cc[VMIN] = 0;
  raw.c_cc[VTIME] = 1;
  
  if (tcsetattr(STDIN_FILENO , TCSAFLUSH, &raw) == -1) die("tcsetattr");
}

char editorReadKey(){
  int nread;
  char c;
  while ((nread == read(STDIN_FILENO, &c, 1)) != 1){
    if(read(STDIN_FILENO , &c, 1) == -1 && errno != EAGAIN) die("read");
    
  }
  return c;
}

void editorProcessKeyPress() {
  char c = editorReadKey();

  switch (c) {
  case CTRL_KEY('q'):
    exit(0);
    break;
  }
}

/*** init ***/
int main(){

  enableRawMode();
  while (1){
    editorProcessKeyPress();
  }
  
  return 0;
}


/*     char c= '\0'; */
/* /\* == 1; *\/ */
/*      if(iscntrl(c)){ */
/*        printf("%d\r\n",c); */
/*      } else { */
/*        printf("%d ('%c')\r\n", c, c); */
/*      } */
/*      if (c != CTRL_KEY('q')) { */
/*        break; */
/*      } */
