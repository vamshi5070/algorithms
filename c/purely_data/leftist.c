#include <stdio.h>
#include <stdlib.h>

struct tree{
    struct tree *left;
    int data;
    struct tree *right;
};

void insert(struct tree **sr,int num){
    if (*sr == NULL){
        *sr = malloc (sizeof( struct tree));
        (*sr) -> left = NULL;
        (*sr) -> data = num;
        (*sr) -> right = NULL;
        return;
    } else if (num < ((*sr) -> data)){
            insert(&((*sr)->left),num);
            return;
    } else {
            insert(&((*sr)->right),num);
            return;
        }
}

void inorder(struct tree *sr){
    if (sr == NULL){
        return;
    } else {
        inorder((sr -> left));
        printf("%d  ",sr -> data);
        inorder((sr -> right));
    }
}

int main(){
    struct tree *bt;
    bt = NULL;
    int req, i = 1, num;
    printf("Specify the no.of items to be inserted:");
    scanf("%d%*[^\n]",&req);
    while(i++ <= req){
        printf("Enter the data:");
        scanf("%d%*[^\n]",&num);
        insert(&bt,num);
    }
    printf("You have entered, now printing..........\n");
    inorder(bt);
    printf("\n");
    return 0;

    

}