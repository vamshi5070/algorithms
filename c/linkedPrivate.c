// private
#include "Node.c"
#include "stdio.h"

#include "linkedList.h"

// traverse till a point
struct Node_int * iterate_int(int index, struct LinkedList_int *linked_list){
  int i;
  if (index < 0 || index >= linked_list -> length){
    printf("Index out of bound... \n");
    exit(9);
  }
  struct Node_int  *cursor = linked_list -> head;
  for (i = 0; i < index; i++){
    cursor = cursor -> next;
  }
  return cursor;
}

struct Node_int * create_node_int(int data){
  struct Node_int *new_node_address = (struct Node_int *)malloc(sizeof(struct Node_int ));
  struct Node_int new_node_instance;
  new_node_instance.data = data;
  new_node_instance.next = NULL;
  *new_node_address = new_node_instance;
  return new_node_address;
}

void destroy_node_int(struct Node_int *node_to_destroy){
  free(node_to_destroy);
}
