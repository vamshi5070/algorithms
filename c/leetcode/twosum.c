#include <stdio.h>
#include <stdlib.h>

int* twoSum(int* nums, int numsSize, int target, int* returnSize){
  int *arr;
  arr = malloc(*returnSize+1);
  for (int i = 0; i < numsSize-1; i++){
    for (int j=i+1; j <= numsSize-1; j++){
      if (nums[i] + nums[j] == target) {
	arr[0] = i;
	arr[1] = j;
      }
    }
  }
  return arr;
}

void printArray(int* arr,int size){
  int i;

  for (i = 0; i < size; i++){
    printf("%d ",arr[i]);
  }
  printf("\n");
}


int main(){
  int inputArr[4] = {3,2,4};
  printArray(twoSum(inputArr,3,6,2),2);
  /* printf("%"); */
}
