#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int reverse_digits(int x){
  int arr[4];
  int i=0;
  int num = x;
  int res_num = 0;
  while (num != 0){
    int temp = num % 10;
    num = num / 10;
    res_num = res_num * 10 + temp;
  }
  return res_num;
}

bool isPalindrome(int x){
  if (x < 0 ){
      return false;
    } else {
    return (reverse_digits(x) == x);
  }
}

int main(){
  printf("%d",reverse_digits(121));
  printf("%b",isPalindrome(122));
  /* int inputArr[4] = {3,2,4}; */
  /* printArray(twoSum(inputArr,3,6,2),2); */
  /* printf("%"); */
}
