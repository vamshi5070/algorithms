#include "linkedList.h"
#include "linkedPublic.c"

int main(){
  struct LinkedList_int list = linked_list_int_init();
  int arr[5] = {5,4,3,2,1};
  int size = 5;
  printf("Inserting..... \n");
  for(int i=0; i < size; i++){
    insert_node_int(i,arr[i],&list);
  }
  printf("Printing............... \n");
  for(int i = 0; i < size;i++){
    int temp = retrieve_data_int(i,&list);
    printf("%d ",temp);
  }
  
  retrieve_data_int(100,&list);

}
