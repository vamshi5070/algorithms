struct Node* arrayToLinkedList(int arr[], int size, struct Node** last) {
    struct Node* head = NULL;
    struct Node* pointer = NULL;
    struct Node* temp = NULL;
    printf("welcome \n");
    if (size == 0) {
        head = NULL;
        *last = NULL;
        return;
    }

    // Create the first node
    head = createNode(arr[0]);
    pointer = head;

    // Create and link the remaining nodes
    for (int i = 1; i < size; i++) {
        temp = createNode(arr[i]);
        (pointer) -> next = temp;
        temp -> prev = pointer;
        pointer = temp;
    }
    (*last) = pointer;
    return head;
}
