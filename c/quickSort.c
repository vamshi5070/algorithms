#include "stdio.h"

void printArray(int* arr,int size){
  int i;

  for (i = 0; i < size; i++){
    printf("%d ",arr[i]);
  }
  printf("\n");
}

void quickSort(int* arr,int size){
  if (size <= 1){
    return;// array is unchanged
  } else {
    /* printf("1 \n"); */
    int leftArr[size],rightArr[size],size1=0,size2=0;
    int pivot = arr[0];
    for (int i = 1; i < size; i++){
      if(pivot >= arr[i]){
	leftArr[size1] = arr[i];
	/* printf("arr 1: %d \n",arr[i]); */
	size1 = size1 + 1;
      } else {
	/* printf("arr 2: %d \n",arr[i]); */
	rightArr[size2] = arr[i];
	size2 = size2 + 1;
      }
    }
    /* printf("Length of arr 1 %d \n",size1); */
    quickSort(leftArr,size1);
    quickSort(rightArr,size2);

          //join
    int i = 0;
    for(i = 0; i < size1; i++){
      arr[i] = leftArr[i];
    }
    arr[i] = pivot;
    i = i + 1;
    for(int j=i; j < i+size2; j++ ){
      arr[j] = rightArr[j-i];
    }
  }
}

int main(){
  int arr[8] = {100,99,98,5,4,3,2,1};
  int size = 8;
  quickSort(arr,size);
  printArray(arr,size);
  /* printf("%d \n",); */
}
