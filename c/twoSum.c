#include "stdio.h"
#include "stdlib.h"

int* twoSum(int* nums, int numsSize, int target, int* returnSize){
    int* res = malloc(sizeof(int)*2);
    int newArr[numsSize],k=0;
    for (int i = 0; i < numsSize; i++){
      if (nums[i] > target){
	continue;
      } else {
	newArr[k] = nums[i];
	k = k + 1;
      }
    }
    for (int i =0; i < k;i++){
      int key = i;
      /* int id  */
      for (int j = 0; j < k;j++){
	if (j == key){
	  continue;
	} else {
	  if(newArr[j] + newArr[key] == target){
	    res[0] = j;
	    res[1] = key;
	    break;
	  } else {
	    continue;
	  }
	}
      }
    }
    return res;
}

int main(){
  /* int arr[4] = {2,7,11,15}; */
  int arr[3] = {3,2,4};
  int returnSize = sizeof(int)*2;
  int* res = twoSum(arr,3,6,&returnSize);
  printf("%d %d",res[0],res[1]);
}
